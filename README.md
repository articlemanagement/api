

# Article Management API

## Develop

You can use a .env file to facilitate the development. see an exemple [here](.env.exemple).

## Run on docker

```bash
docker run --name amazing-app \
           -p 8080:1515 \
           -v /host/path/to/project/sqlite-data:/usr/src/app/sqlite-data \
           -e PORT=1515 \
           -e AUTH0_ISSUER=https://your-auth0-domain.com/ \
           -e AUTH0_AUDIENCE=https://your-auth0-domain.com/api/v2/ \
           sylvaindupuy/article-management-api:latest
```

> Your data will be persisted in the 'sqlite-data' folder