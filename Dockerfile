FROM node:14-alpine

ENV NODE_ENV='production'

# Create app directory
WORKDIR /usr/src/app

# install needed package
RUN apk add git python g++ make

# copy 'package.json'
COPY package.json /usr/src/app
COPY package-lock.json /usr/src/app

# install dependencies
RUN npm ci

# copy compiled project files
COPY build/ /usr/src/app/build/

# copy typeorm config
COPY ormconfig.json /usr/src/app

# launch app on startup
CMD ["sh", "-c", "node build/index.js"]
