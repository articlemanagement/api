import express from 'express';
import "reflect-metadata";

import { fridgesRouter } from './routes/fridges.router';
import { articlesRouter } from './routes/articles.router';
import { checkJwt } from './middleware/auth.middleware';

import { createConnection } from "typeorm";

createConnection().then(async () => {
    const app = express();
    const port = process.env.PORT || 8080;

    app.use(checkJwt);
    app.use(express.json())

    app.use('/fridges', fridgesRouter);
    app.use('/articles', articlesRouter);

    app.listen(port, () => {
        // tslint:disable-next-line:no-console
        console.log(`server started port ${port}`);
    });
});