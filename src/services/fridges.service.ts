import Fridge from "../entity/Fridge";
import { getManager } from "typeorm";
import { ArticleToFridge } from "../entity/ArticleToFridge";

function mapFridgeArticles(fridge: Fridge) {
    // map relation to a simple object
    fridge.articles = fridge.articleToFridge.map((articleToFridge: ArticleToFridge) => {
        return {
            barCode: articleToFridge.article.barCode,
            name: articleToFridge.article.name,
            photo: articleToFridge.article.photo,
            quantity: articleToFridge.quantity,
            expirationDate: articleToFridge.expirationDate
        }
    });

    // remove relations from selection
    delete fridge.articleToFridge;
}

export const findAll = async (userId: string): Promise<Fridge[]> => {
    const fridgeRepository = getManager().getRepository(Fridge);
    const fridges = await fridgeRepository.find({ where: { userId }, relations: ["articleToFridge", "articleToFridge.article"] });

    fridges.map(mapFridgeArticles);

    return fridges;
};

export const find = async (userId: string, id: number): Promise<Fridge> => {
    const fridgeRepository = getManager().getRepository(Fridge);

    const fridge = await fridgeRepository.findOne({ userId, id }, { relations: ["articleToFridge", "articleToFridge.article"] });

    if (fridge === undefined) {
        throw new Error('No record found');
    }

    mapFridgeArticles(fridge);

    return fridge;
};

export const create = async (userId: string, newFridge: Fridge): Promise<Fridge> => {
    const fridgeRepository = getManager().getRepository(Fridge);
    const fridge = await fridgeRepository.findOne({ userId, name: newFridge.name });

    if (fridge !== undefined) {
        throw new Error('Fridge already exist');
    }

    // set user id from jwt
    newFridge.userId = userId;

    return fridgeRepository.save(newFridge);
};

export const addArticleToFridge = async (userId: string, fridgeId: number, articleBarCode: string, quantity: number, expirationDate: string): Promise<ArticleToFridge> => {
    const fridgeRepository = getManager().getRepository(Fridge);
    const articleToFridgeRepository = getManager().getRepository(ArticleToFridge);
    const fridge = await fridgeRepository.findOne({ userId, id: fridgeId });

    if (fridge === undefined || quantity === undefined || expirationDate === undefined) {
        throw new Error('Invalid data');
    }

    let articleToFridge = await articleToFridgeRepository.findOne({ articleBarCode, fridgeId, expirationDate });

    if (articleToFridge === undefined) {
        // create it
        articleToFridge = new ArticleToFridge();
        articleToFridge.articleBarCode = articleBarCode;
        articleToFridge.fridgeId = fridgeId;
        articleToFridge.quantity = quantity;
        articleToFridge.expirationDate = new Date(expirationDate);
    } else {
        // update
        articleToFridge.quantity += quantity;
    }

    return articleToFridgeRepository.save(articleToFridge);
}

export const deleteArticleFromFridge = async (userId: string, fridgeId: number, articleBarCode: string, quantity: number, expirationDate: string): Promise<void> => {
    const fridgeRepository = getManager().getRepository(Fridge);
    const articleToFridgeRepository = getManager().getRepository(ArticleToFridge);
    const fridge = await fridgeRepository.findOne({ userId, id: fridgeId });

    // verify if fridge corresponds to the user
    if (fridge === undefined || quantity === undefined || expirationDate === undefined) {
        throw new Error('Invalid data');
    }

    const articleToFridge = await articleToFridgeRepository.findOne({ articleBarCode, fridgeId, expirationDate });

    if (articleToFridge === undefined) {
        throw new Error('Invalid data');
    }

    articleToFridge.quantity -= quantity;

    if (articleToFridge.quantity <= 0) {
        // delete
        await articleToFridgeRepository.remove(articleToFridge);
    } else {
        // update
        await articleToFridgeRepository.save(articleToFridge);
    }
}