import { getManager } from "typeorm";
import Article from "../entity/Article";

export const findAll = async (): Promise<Article[]> => {
    const articleRepository = getManager().getRepository(Article);
    return articleRepository.find();
};

export const find = async (barCode: string): Promise<any> => {
    const articleRepository = getManager().getRepository(Article);
    return articleRepository.findOne(barCode);
};

export const create = async (newArticle: Article): Promise<Article> => {
    const articleRepository = getManager().getRepository(Article);
    const article = await articleRepository.findOne(newArticle.barCode);

    if (article !== undefined) {
        throw new Error('Article already exist');
    }

    return articleRepository.save(newArticle);
};

export const update = async (updatedArticle: Article): Promise<Article> => {
    const articleRepository = getManager().getRepository(Article);
    const article = await articleRepository.findOne(updatedArticle.barCode);

    if (article === undefined) {
        throw new Error('No record found to update');
    }

    if (updatedArticle.name && updatedArticle.name !== '') {
        article.name = updatedArticle.name;
    }

    if (updatedArticle.photo && updatedArticle.photo !== '') {
        article.photo = updatedArticle.photo;
    }

    return articleRepository.save(article);
};

export const remove = async (barCode: string): Promise<Article> => {
    const articleRepository = getManager().getRepository(Article);
    const article = await articleRepository.findOne(barCode);

    if (article === undefined) {
        throw new Error('No record found to delete');
    }

    return articleRepository.remove(article);
};