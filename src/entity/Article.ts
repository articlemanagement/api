import { Entity, Column, PrimaryColumn, OneToMany } from "typeorm";
import { ArticleToFridge } from "./ArticleToFridge";

@Entity()
export default class Article {

    @PrimaryColumn({
        length: 13
    })
    barCode!: string;

    @Column()
    name!: string;

    @Column()
    photo!: string;

    @OneToMany(type => ArticleToFridge, articleToFridge => articleToFridge.article)
    public articleToFridge!: ArticleToFridge[];
}