import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { ArticleToFridge } from "./ArticleToFridge";

@Entity()
export default class Fridge {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    name!: string;

    @Column({ select: false })
    userId!: string;

    @OneToMany(() => ArticleToFridge, articleToFridge => articleToFridge.fridge)
    public articleToFridge!: ArticleToFridge[];

    articles!: any
}