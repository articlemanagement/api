import { Entity, Column, ManyToOne, PrimaryColumn } from "typeorm";
import Article from "./Article";
import Fridge from "./Fridge";

@Entity()
export class ArticleToFridge {

    @PrimaryColumn({ length: 13 })
    public articleBarCode!: string;

    @PrimaryColumn()
    public fridgeId!: number;

    @PrimaryColumn({ type: 'date' })
    public expirationDate!: Date;

    @Column()
    public quantity!: number;

    @ManyToOne(() => Article, article => article.articleToFridge)
    public article!: Article;

    @ManyToOne(() => Fridge, fridge => fridge.articleToFridge)
    public fridge!: Fridge;
}