import express, { Response } from 'express';
import * as FridgeService from '../services/fridges.service';

import Fridge from '../entity/Fridge';

export const fridgesRouter = express.Router();

fridgesRouter.get('/', async (req: any, res: Response) => {
    try {
        const items: Fridge[] = await FridgeService.findAll(req.user.sub);

        res.status(200).send(items);
    } catch (e) {
        res.status(404).send(e.message);
    }
});

fridgesRouter.post('/', async (req: any, res: Response) => {
    try {
        const fridge: Fridge = req.body.fridge;

        const item = await FridgeService.create(req.user.sub, fridge);

        res.status(201).send(item);
    } catch (e) {
        res.status(404).send(e.message);
    }
});

fridgesRouter.get('/:fridgeId', async (req: any, res: Response) => {
    const fridgeId: number = parseInt(req.params.fridgeId, 10);

    try {
        const item: Fridge = await FridgeService.find(req.user.sub, fridgeId);

        res.status(200).send(item);
    } catch (e) {
        res.status(404).send(e.message);
    }
});

fridgesRouter.post('/:fridgeId/:barCode', async (req: any, res: Response) => {
    try {
        const fridgeId: number = parseInt(req.params.fridgeId, 10);
        const barCode: string = req.params.barCode;
        const quantity: number = req.body.quantity;
        const expirationDate: string = req.body.expirationDate;

        await FridgeService.addArticleToFridge(req.user.sub, fridgeId, barCode, Math.abs(quantity), expirationDate);

        res.sendStatus(201);
    } catch (e) {
        res.status(404).send(e.message);
    }
});

fridgesRouter.delete('/:fridgeId/:barCode', async (req: any, res: Response) => {
    try {
        const fridgeId: number = parseInt(req.params.fridgeId, 10);
        const barCode: string = req.params.barCode;
        const quantity: number = req.body.quantity;
        const expirationDate: string = req.body.expirationDate;

        await FridgeService.deleteArticleFromFridge(req.user.sub, fridgeId, barCode, Math.abs(quantity), expirationDate);

        res.sendStatus(200);
    } catch (e) {
        res.status(500).send(e.message);
    }
});