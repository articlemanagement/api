import express, { Request, Response } from 'express';
import * as ArticleController from '../services/articles.service';
import Article from '../entity/Article';

export const articlesRouter = express.Router();

articlesRouter.get('/', async (req: Request, res: Response) => {
    try {
        const items: Article[] = await ArticleController.findAll();

        res.status(200).send(items);
    } catch (e) {
        res.status(404).send(e.message);
    }
});

articlesRouter.get('/:barCode', async (req: Request, res: Response) => {
    const barCode: string = req.params.barCode;

    try {
        const item: Article = await ArticleController.find(barCode);

        res.status(200).send(item);
    } catch (e) {
        res.status(404).send(e.message);
    }
});

articlesRouter.post('/', async (req: Request, res: Response) => {
    try {
        const article: Article = req.body.article;

        const item: Article = await ArticleController.create(article);

        res.status(201).send(item);
    } catch (e) {
        res.status(404).send(e.message);
    }
});

articlesRouter.put('/', async (req: Request, res: Response) => {
    try {
        const article: any = req.body.article;

        const item: Article = await ArticleController.update(article);

        res.status(200).send(item);
    } catch (e) {
        res.status(500).send(e.message);
    }
});

articlesRouter.delete('/:barCode', async (req: Request, res: Response) => {
    try {
        const barCode: string = req.params.barCode;
        await ArticleController.remove(barCode);

        res.sendStatus(200);
    } catch (e) {
        res.status(500).send(e.message);
    }
});